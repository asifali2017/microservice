package com.currency.conversion.service.proxy;


import com.currency.conversion.service.model.CurrencyExchange;
//import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "currency-exchange" ,url = "http://localhost:8000/")
//@FeignClient(name = "CURRENCY-EXCHANGE-SERVICE")
//@FeignClient(contextId ="exchangeService",name = "zuul-api-gateway")
@FeignClient(contextId ="exchangeService",name = "cloud-api-gateway")
//@RibbonClient(name = "CURRENCY-EXCHANGE-SERVICE")
public interface CurrencyExchangeClient {
    @GetMapping("/currency-exchange-service/currency/exchange/from/{from}/to/{to}/")
    public ResponseEntity<CurrencyExchange> retrieveCurrencyExchange(@PathVariable("from") String from, @PathVariable("to") String to);

}
