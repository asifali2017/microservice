package com.currency.conversion.service.proxy;

import com.currency.conversion.service.model.CurrencyExchange;
import com.currency.conversion.service.model.EcbExchangeRateDto;
//import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "exchange-rate-service")
@FeignClient(contextId ="exchangeRate",name = "zuul-api-gateway")
//@RibbonClient(name = "exchange-rate-service")
public interface CurrencyRateClient {
    @GetMapping("/exchange-rate-service/ecb/exchange/{amount}/{from}/{to}")
    public ResponseEntity<EcbExchangeRateDto> retrieveCurrencyRate(@PathVariable("amount") Double amount,@PathVariable("from") String from, @PathVariable("to") String to);
}
