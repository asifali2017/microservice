package com.currency.conversion.service.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Resilience4jController {

    @GetMapping("/sample")
    public String sampleAPI(){
        return "Hello World";
    }

}
