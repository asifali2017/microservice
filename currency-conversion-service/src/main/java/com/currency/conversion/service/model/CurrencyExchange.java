package com.currency.conversion.service.model;

public class CurrencyExchange {
    private Integer id;
    private String from;
    private String to;
    private Double amount;
    private Integer quantity;
    private Integer port;

    public CurrencyExchange() {
    }

    public CurrencyExchange(int id, String from, String to, Double amount,Integer port) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.port = port;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
