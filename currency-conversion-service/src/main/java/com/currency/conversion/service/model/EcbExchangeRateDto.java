package com.currency.conversion.service.model;

public class EcbExchangeRateDto {
    private String from;
    private Double amount;
    private String to;
    private Double rate;

    public EcbExchangeRateDto(String from, Double amount, String to, Double rate) {
        this.from = from;
        this.amount = amount;
        this.to = to;
        this.rate = rate;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
