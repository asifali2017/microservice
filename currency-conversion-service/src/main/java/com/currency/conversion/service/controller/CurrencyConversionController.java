package com.currency.conversion.service.controller;

import com.currency.conversion.service.model.CurrencyExchange;
import com.currency.conversion.service.model.EcbExchangeRateDto;
import com.currency.conversion.service.proxy.CurrencyExchangeClient;
import com.currency.conversion.service.proxy.CurrencyExchangeClient2;
import com.currency.conversion.service.proxy.CurrencyRateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("/currency-conversion")
public class CurrencyConversionController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CurrencyExchangeClient currencyExchangeClient;

    @Autowired
    private CurrencyExchangeClient2 currencyExchangeClient2;

    @Autowired
    private CurrencyRateClient currencyRateClient;

//    @GetMapping("/currency/conversion/from/{from}/to/{to}/quantity/{quantity}")
//    public ResponseEntity<CurrencyExchange> retrieveCurrencyExchange(@PathVariable("from") String from, @PathVariable("to") String to, @PathVariable("quantity") Integer quantity){
//        Map<String,String> httpParam = new HashMap<>();
//
//        httpParam.put("from",from);
//        httpParam.put("to",to);
//
//        ResponseEntity<CurrencyExchange> currencyExchangeResponseEntity =
//                restTemplate.getForEntity("http://localhost:8000/currency/exchange/from/{from}/to/{to}",CurrencyExchange.class,httpParam);
//
//        CurrencyExchange currencyExchange = currencyExchangeResponseEntity.getBody();
//
//        currencyExchange.setAmount(quantity*currencyExchange.getAmount());
//        currencyExchange.setQuantity(quantity);
//
//        return ResponseEntity.ok(currencyExchange);
//    }

    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }

    @GetMapping("/currency/conversion/from/{from}/to/{to}/quantity/{quantity}")
    public ResponseEntity<CurrencyExchange> retrieveCurrencyExchange(@PathVariable("from") String from, @PathVariable("to") String to, @PathVariable("quantity") Integer quantity){

       ResponseEntity<CurrencyExchange> currencyExchangeResponseEntity = currencyExchangeClient.retrieveCurrencyExchange(from,to);

       CurrencyExchange currencyExchange = currencyExchangeResponseEntity.getBody();
       logger.info("{}",currencyExchange);

       currencyExchange.setAmount(quantity*currencyExchange.getAmount());
       currencyExchange.setQuantity(quantity);

        return ResponseEntity.ok(currencyExchange);
    }


    @GetMapping("/currency/conversion2/from/{from}/to/{to}/quantity/{quantity}")
    public ResponseEntity<CurrencyExchange> retrieveCurrencyExchange2(@PathVariable("from") String from, @PathVariable("to") String to, @PathVariable("quantity") Integer quantity){

        ResponseEntity<CurrencyExchange> currencyExchangeResponseEntity = currencyExchangeClient2.retrieveCurrencyExchange(from,to);

        CurrencyExchange currencyExchange = currencyExchangeResponseEntity.getBody();
        logger.info("{}",currencyExchange);

        currencyExchange.setAmount(quantity*currencyExchange.getAmount());
        currencyExchange.setQuantity(quantity);

        return ResponseEntity.ok(currencyExchange);
    }

    @GetMapping("/currency/rate/from/{from}/to/{to}/quantity/{quantity}")
    public ResponseEntity<EcbExchangeRateDto> retrieveCurrencyRate(@PathVariable("from") String from, @PathVariable("to") String to, @PathVariable("quantity") Double quantity){

        ResponseEntity<EcbExchangeRateDto> currencyRateResponseEntity = currencyRateClient.retrieveCurrencyRate(quantity,from,to);

        EcbExchangeRateDto ecbExchangeRateDto = currencyRateResponseEntity.getBody();

        return ResponseEntity.ok(ecbExchangeRateDto);
    }


    @GetMapping("currency-conversion/checking/{name}")
    public String retrieveCurrencyRate2(@PathVariable("name") String name){
        return "Hello World"+name;
    }


}
