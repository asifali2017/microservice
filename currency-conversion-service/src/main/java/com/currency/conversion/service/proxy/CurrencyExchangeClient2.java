package com.currency.conversion.service.proxy;


import com.currency.conversion.service.model.CurrencyExchange;
//import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "currency-exchange" ,url = "http://localhost:8000/")
//@FeignClient(name = "CURRENCY-EXCHANGE-SERVICE")
//@RibbonClient(name = "CURRENCY-EXCHANGE-SERVICE")
//@FeignClient(contextId ="exchangeService",name = "zuul-api-gateway")
//As we have used spring cloud load balancer instead of ribbon
@FeignClient(name = "currency-exchange-service")
public interface CurrencyExchangeClient2 {
//    @GetMapping("/currency-exchange-service/currency/exchange/from/{from}/to/{to}/")
//    public ResponseEntity<CurrencyExchange> retrieveCurrencyExchange(@PathVariable("from") String from, @PathVariable("to") String to);
   @GetMapping("/currency/exchange/from/{from}/to/{to}/")
    public ResponseEntity<CurrencyExchange> retrieveCurrencyExchange(@PathVariable("from") String from, @PathVariable("to") String to);

}
