package currency.exchange.service.services;

import currency.exchange.service.exceptions.NotFoundException;
import currency.exchange.service.model.CurrencyExchange;
import currency.exchange.service.repository.CurrencyExchangeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyExchangeService {
    @Autowired
    private CurrencyExchangeRepo currencyExchangeRepo;

    public CurrencyExchange getExchangeCurrency(String from,String to) throws NotFoundException{

        CurrencyExchange currencyExchange = currencyExchangeRepo.findAllByFromAndTo(from,to);

        if(currencyExchange == null){
            throw new NotFoundException();
        }
        return currencyExchange;
    }
}
