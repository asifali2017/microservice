package currency.exchange.service.repository;

import currency.exchange.service.model.CurrencyExchange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CurrencyExchangeRepo extends JpaRepository<CurrencyExchange,Integer> {
    public CurrencyExchange findAllByFromAndTo(String from,String to);
}
