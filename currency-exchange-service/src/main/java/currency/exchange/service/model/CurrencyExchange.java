package currency.exchange.service.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="currency_exchange")
public class CurrencyExchange {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;
    @Column(name="exchange_from")
    private String from;
    @Column(name="exchange_to")
    private String to;
    @Column(name = "exchange_amount")
    private Double amount;
    @Transient
    private int port;

    public CurrencyExchange() {
    }

    public CurrencyExchange(int id, String from, String to, Double amount,int port) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.port = port;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
