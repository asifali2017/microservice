package currency.exchange.service.controller;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CircuitBreakerController {

    String cache = null;

    @GetMapping("/circuit")
    @CircuitBreaker(name = "default",fallbackMethod = "handleCircuit")
    public String getCurrencies(){
        ResponseEntity<String> str = new RestTemplate().getForEntity("http://localhost:8081/currencies",String.class);
        cache = str.getBody();
        return cache+" From db";
    }

    public String handleCircuit(Exception e){
        return cache +" From cache";
    }


}
