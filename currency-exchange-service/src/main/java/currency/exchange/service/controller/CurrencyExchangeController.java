package currency.exchange.service.controller;

import currency.exchange.service.exceptions.NotFoundException;
import currency.exchange.service.model.CurrencyExchange;
import currency.exchange.service.repository.CurrencyExchangeRepo;
import currency.exchange.service.services.CurrencyExchangeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class CurrencyExchangeController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CurrencyExchangeService currencyExchangeService;

    @Autowired
    private ServerProperties serverProperties;

    @GetMapping("/currency/exchange/from/{from}/to/{to}")
    public ResponseEntity<CurrencyExchange> retrieveCurrencyExchange(@PathVariable("from") String from, @PathVariable("to") String to){
    try {
        CurrencyExchange currencyExchange = currencyExchangeService.getExchangeCurrency(from, to);
        logger.info("{}",currencyExchange);
        currencyExchange.setPort(serverProperties.getPort());
        return ResponseEntity.ok(currencyExchange);

    }
    catch (NotFoundException ex){
        throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Currency Exchange Not Found");
    }

    }
}
