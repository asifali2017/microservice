package currency.exchange.service.controller;

import io.github.resilience4j.retry.annotation.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Resilience4jController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/sample")
    @Retry(name = "default", fallbackMethod = "handleFailure")
    public String getSample(){
        logger.info("Sample Api Requested");
        ResponseEntity<String> str = new RestTemplate().getForEntity("localhost:8005/abc",String.class);
        return str.getBody();
    }

    public String handleFailure(Exception e){
        return "fallback";
    }
}
