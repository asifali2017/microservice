package com.cloud.api.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ApiGatewayConfiguration {

    @Bean
    public RouteLocator getRouteLocator(RouteLocatorBuilder builder){
        return
                builder.routes().
                        route(route->route
                                .path("/currency-conversion/**")
                                .uri("lb://currency-conversion-service")).
                        build();
    }
}
